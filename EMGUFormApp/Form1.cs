﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using Color = System.Drawing.Color;


namespace EMGUFormApp
{
    public partial class Form1 : Form
    {
        public VideoCapture WebCam { get; set; }
        public EigenFaceRecognizer faceRecognizer { get; set; }
        public CascadeClassifier faceClassifier { get; set; }
        public CascadeClassifier eyeClassifier { get; set; }

        public Mat Frame { get; set; }

        public bool ShowFaceSquare = true;
        public bool ShowEyeSquare = true;
        private bool IsTrainning = false;
        private System.Timers.Timer TrainningTimer;
        private int TimeLimit = 10;
        private int TimerCounter = 0;
        private List<Image<Gray, byte>> trainningFaces;
        private List<int> TrainningIds;
        private int TrainningUserId = 0;
        private string YMLPATH = @"../../output/trainningData.yml";
        private int ProcessedImageWidth = 128;
        private int ProcessedImageHeight = 150;

        private int CurrentUserID = 0;

        private void SetCurrentID(int value)
        {
            CurrentUserID = value;

            UserIDLabel.Invoke((MethodInvoker)delegate
            {
                if (value > 0)
                {
                    UserIDLabel.Text = string.Format("User ID: {0}", value);
                }
                else
                {
                    UserIDLabel.Text = "User ID: (not found)";
                }
            });
            
        }

        public Form1()
        {
            InitializeComponent();
            faceRecognizer = new EigenFaceRecognizer(80, double.PositiveInfinity);
            if (System.IO.File.Exists(YMLPATH))
            {
               faceRecognizer.Read(YMLPATH);
            }
            var facialClassifierPath = System.IO.Path.GetFullPath("../../data/haarcascades/haarcascade_frontalface_default.xml");
            var eyeClassifierPath = System.IO.Path.GetFullPath("../../data/haarcascades/haarcascade_eye.xml");

            faceClassifier = new CascadeClassifier(facialClassifierPath);
            eyeClassifier = new CascadeClassifier(eyeClassifierPath);

            Frame = new Mat();

            try
            {
                var saved_id = (int)(Properties.Settings.Default["LAST_USED_TRAINING_ID"]);
                if (saved_id > 0)
                {
                    TrainningUserId = saved_id;
                }
            }
            catch (Exception )
            {

            }
            

            BeginWebCam();
        }

        

        private void BeginWebCam()
        {
            if (WebCam == null)
            {
                WebCam = new VideoCapture(1);
            }
            
             WebCam.ImageGrabbed += Camera_Image_Grabbed;
             WebCam.Start();
        }

        private void StartTrainning()
        {
            IsTrainning = true;
            TimerCounter = 0;
            trainningFaces = new List<Image<Gray, byte>>();
            TrainningUserId++;
            TrainningIds = new List<int>();

             TrainningTimer = new System.Timers.Timer();
            TrainningTimer.Elapsed += new ElapsedEventHandler(Trainning_Timer_Tick);
            TrainningTimer.Interval = 500;
            TrainningTimer.Enabled = true;
            TrainningTimer.Start();
            Console.WriteLine("Training start");
        }

        private void Trainning_Timer_Tick(object sender, EventArgs e)
        {
            WebCam.Retrieve(Frame);
            var imageFrame = Frame.ToImage<Gray, byte>();

            if (TimerCounter < TimeLimit) {
                TimerCounter++;

                if (imageFrame != null)
                {
                    var faces_rects = faceClassifier.DetectMultiScale(imageFrame, 1.3, 5);
                    if (faces_rects.Count() > 0)
                    {
                        var processImage = imageFrame.Copy(faces_rects[0]).Resize(ProcessedImageWidth, ProcessedImageHeight, Emgu.CV.CvEnum.Inter.Cubic);
                        trainningFaces.Add(processImage);

                        TrainningIds.Add(TrainningUserId);
                    }
                }
            }else
            {
                TrainningTimer.Enabled = false;
                TrainningTimer.Stop();
                if (trainningFaces.Count() > 0)
                {
                    try
                    {
                        faceRecognizer.Train(trainningFaces.ToArray(), TrainningIds.ToArray());
                        faceRecognizer.Write(YMLPATH);
                        Properties.Settings.Default["LAST_USED_TRAINING_ID"] = TrainningUserId;
                        Properties.Settings.Default.Save(); // Saves settings in application configuration file

                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine("Trainning ex: {0}", ex.Message);
                    }
                    
                }
               
                IsTrainning = false;
                TrainningTimer = null;
                Console.WriteLine("Training Ends");
            }
        }

        private int PredictUserIDFromGrayImage(Image <Gray, byte> imageFrame, Rectangle[] faceRects)
        {
            // Check if trainning file exist
            if (System.IO.File.Exists(YMLPATH) == false) {
                return 0;
            }
            foreach (var faceRect in faceRects)
            {
                if (faceRects.Count() > 0)
                {
                    try
                    {
                        var processImage = imageFrame.Copy(faceRect).Resize(ProcessedImageWidth, ProcessedImageHeight, Emgu.CV.CvEnum.Inter.Cubic);
                        var result = faceRecognizer.Predict(processImage);

                        if (result.Label > 0)
                        {
                            return result.Label;
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine("Exception: {0}", ex);
                    }
                    
                }
            }

            return 0;
        }

        private void Camera_Image_Grabbed(object sender, EventArgs e)
        {
            WebCam.Retrieve(Frame);
            var imgFrame = Frame.ToImage<Bgr, byte>();
            SetCurrentID(0);
            if (imgFrame != null)
            {
                var grayFrame = imgFrame.Convert<Gray, byte>();
                var faces = faceClassifier.DetectMultiScale(grayFrame, 1.3, 5);
                var eyes = eyeClassifier.DetectMultiScale(grayFrame, 1.3, 5);
                
                if (ShowFaceSquare)
                {
                    foreach (var face in faces)
                    {
                        imgFrame.Draw(face, new Bgr(Color.BurlyWood), 3);
                    }
                }

                if (ShowEyeSquare)
                {
                    foreach (var face in eyes)
                    {
                        imgFrame.Draw(face, new Bgr(Color.Yellow), 3);
                    }
                }
                
                CamBox.Image = imgFrame.Flip(Emgu.CV.CvEnum.FlipType.Horizontal).ToBitmap();
                var DetectedFace = PredictUserIDFromGrayImage(grayFrame, faces);
                SetCurrentID(DetectedFace);

                if (DetectedFace > 0)
                {
                    Console.WriteLine("User With ID {0} found", DetectedFace);
                } else 
                if (IsTrainning == false )
                {
                    StartTrainning();
                }
            }
        }
    }
}
