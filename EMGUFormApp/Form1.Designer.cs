﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Input;
using Emgu;
using Emgu.CV;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using Color = System.Drawing.Color;

namespace EMGUFormApp
{
    partial class Form1
    {
       
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CamBox = new System.Windows.Forms.PictureBox();
            this.UserIDLabel = new System.Windows.Forms.Label();
            this.TrainingLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CamBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CamBox
            // 
            this.CamBox.AccessibleName = "CamBox";
            this.CamBox.Location = new System.Drawing.Point(12, 25);
            this.CamBox.Name = "CamBox";
            this.CamBox.Size = new System.Drawing.Size(640, 360);
            this.CamBox.TabIndex = 0;
            this.CamBox.TabStop = false;
            // 
            // UserIDLabel
            // 
            this.UserIDLabel.AutoSize = true;
            this.UserIDLabel.Location = new System.Drawing.Point(670, 58);
            this.UserIDLabel.Name = "UserIDLabel";
            this.UserIDLabel.Size = new System.Drawing.Size(100, 13);
            this.UserIDLabel.TabIndex = 1;
            this.UserIDLabel.Text = "User ID: (not found)";
            // 
            // TrainingLabel
            // 
            this.TrainingLabel.AutoSize = true;
            this.TrainingLabel.Location = new System.Drawing.Point(670, 25);
            this.TrainingLabel.Name = "TrainingLabel";
            this.TrainingLabel.Size = new System.Drawing.Size(48, 13);
            this.TrainingLabel.TabIndex = 2;
            this.TrainingLabel.Text = "Training:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TrainingLabel);
            this.Controls.Add(this.UserIDLabel);
            this.Controls.Add(this.CamBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.CamBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox CamBox;
        private System.Windows.Forms.Label UserIDLabel;
        private System.Windows.Forms.Label TrainingLabel;
    }


}



    